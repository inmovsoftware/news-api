<?php

namespace Inmovsoftware\NewsApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsCategory extends Model
{
    use SoftDeletes;
    protected $table = "it_categories_news";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
    protected $fillable = ['id_business_id', 'name', 'status'];

    public function News()
    {
        return $this->hasMany('Inmovsoftware\NewsApi\Models\V1\News', 'it_categories_news_id', 'id');

    }

    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".name", 'like', "%$param%");
    }

    public static function scopethisUser(){
        $$query->it_users = auth()->user()->id;
    }



}
